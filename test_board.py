import pytest
import pygame
import src.person as person
import src.board as board
import src.element as element

@pytest.fixture(scope='session')
def level():
    pygame.init()
    playerList=[person.Player("test name", "./res/blue.png")]
    game=board.Board(playerList, [], './res/test.dat')
    return game

class Test_Board:
    """Class to test the board class"""

    def test_size(self, level):
        assert len(level.grid) == 30
        assert len(level.grid[0]) == 80

    def test_coins(self, level):
        count = 0
        for i in level.elements:
            if type(i) == element.Coin:
                count+=1
        assert count >= 20

    def test_princess(self, level):
        count = 0
        for i in level.elements:
            if type(i) == element.Trophy:
                count+=1
        assert count == 1

    def test_giveSurrounding(self, level):
        #Test case 1 - top-left corner,not touching wall
        surround = level.giveSurrounding((1,1))
        assert surround[8] and not (surround[4] or surround[6] or surround[2])

        #Test case 2 - top-left corner, touching wall
        surround = level.giveSurrounding((0.9,1))
        assert (surround[8] and surround[4]) and not (surround[6] or surround[2])

        #Test case 3 - on ground only on bottom
        surround = level.giveSurrounding((2,8))
        assert surround[2] and not (surround[4] or surround[6] or surround[8])

        #Test case 4 - inbetween two walls
        surround = level.giveSurrounding((2,9))
        assert (surround[4] and surround[6]) and not (surround[5] or surround[8] or surround[2])

        #Test case 5 - standing on top of ladder
        surround = level.giveSurrounding((6,16))
        assert (surround[1] and surround[7] and surround[3]) and not (surround[2] or surround[4] or surround[6] or surround[8])

        #Test case 6 - climbing ladder, on ground
        surround = level.giveSurrounding((6,20))
        assert (surround[2] and surround[5]) and not (surround[4] or surround[6] or surround[8])

        #Test case 7 - climbing ladder, not on ground
        surround = level.giveSurrounding((6,19))
        assert surround[5] and not (surround[2] or surround[4] or surround[6] or surround[8])

        #Test case 8 - mid-air
        surround = level.giveSurrounding((20,5))
        assert surround == [False]*10
