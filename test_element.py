import pytest
import src.person as person
import src.board as board
import src.element as element
import test_board
import time

level=test_board.level

class Test_Element:
    """Class for testing the element class"""
    
    @pytest.fixture(scope='class')
    def item(self, level):
        return level.elements[0]

    def test_resetTime(self, item):
        item.resetTime()
        initial_time=item.time
        item.resetTime()
        final_time=item.time
        assert final_time - initial_time < 10
        item.resetTime()
        initial_time=item.time
        time.sleep(0.1)
        item.resetTime()
        final_time=item.time
        assert final_time - initial_time >= 100

    def test_kill(self, item):
        life=item.isAlive()
        assert life == True
        item.kill()
        life=item.isAlive()
        assert life == False
