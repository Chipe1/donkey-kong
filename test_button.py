import pytest
import src.button as button

class Test_Button:
    """Class to test the button class"""

    @pytest.fixture(scope='class')
    def link(self):
        icon_location="./res/play.png"
        button_instance = button.Button(icon_location, icon_location, (0,0), (100,200))
        return button_instance
    
    def test_onPress(self,link):
        link.onPress(max, 1, 2, 67, -4)
        assert link.run == max

    def test_action(self, link):
        list = [1, 2, 3, 4]
        def my_func(array,i,x):
            array[i]*=x
        link.onPress(my_func, list, 1, -1)
        link.action()
        assert list == [1, -2, 3, 4]
