import pygame
import sys
import src.element as element
import src.person as person
from src.config import *
import random

class Board:
    """Class for managing the current game"""
    def __init__(this,playerList,donkeyList,levelFile=None):
        #Generating the grid
        #if no level file is mentioned, generate a random level
        if levelFile==None:
            this.grid=[[' ']*80 for i in range(30)]
            #Border
            this.grid[0]=['X']*80
            this.grid[-1]=['X']*80
            for i in range(30):
                this.grid[i][0]='X'
                this.grid[i][-1]='X'
            #random part
            this.grid[5]='X'*30+' '*50
            this.grid[4][1]='D'
            this.grid[-2][1]='P'
        #If levelfile is present, load the file
        else:
            this.grid=[]
            f=open(levelFile,'r')
            for line in f.readlines():
                if line[-1]=='\n':
                    line=line[:-1]
                if len(line)!=80:
                    print("'%s' len is %d"%(line,len(line)))
                this.grid.append(list(line))
            f.close()
            
        #Process the level
        this.complete=False
        this.persons=[]
        this.elements=[]
        floorcoord=[]
        coinCount=0
        for i in range(1,30):
            for j in range(1,79):
                if this.grid[i][j]=='X' and this.grid[i-1][j]==' ':
                    floorcoord.append((j,i-1))

        for i in range(30):
            for j in range(80):
                char=this.grid[i][j]
                if char in ('P','D'):
                    this.persons.append((j,i,char))
                    this.grid[i][j]=' '
                elif char in ('C','O','Q'):
                    if char=='C':
                        coinCount+=1
                    this.elements.append((j,i,char))
                    this.grid[i][j]=' '
        
        reqCoin=20+random.randint(0,7)
        while coinCount<reqCoin:
            i=random.randint(0,len(floorcoord)-1)
            this.elements.append(floorcoord[i]+('C',))
            coinCount+=1
            floorcoord.pop(i)

        #Creating the static board surface
        this.ladderImg=None
        this.wallImg=None
        this.coinImg=None
        this.fireballImg=None
        this.trophyImg=None
        this.surface=pygame.Surface((WIDTH,HEIGHT))
        this.surface.fill((0,0,0))
        if not test_mode:
            this.ladderImg=pygame.transform.smoothscale(pygame.image.load(ImageLocation.ladder).convert_alpha(),(int(XMUL),int(YMUL)))
            this.wallImg=pygame.transform.smoothscale(pygame.image.load(ImageLocation.wall).convert_alpha(),(int(XMUL),int(YMUL)))
            img=pygame.image.load(ImageLocation.coin).convert_alpha()
            this.coinImg=pygame.transform.smoothscale(img,(int(XMUL*(img.get_width()/img.get_height())),int(YMUL)))
            img=pygame.image.load(ImageLocation.fireball).convert_alpha()
            this.fireballImg=pygame.transform.smoothscale(img,(int(XMUL*(img.get_width()/img.get_height())),int(YMUL)))
            img=pygame.image.load(ImageLocation.trophy).convert_alpha()
            this.trophyImg=pygame.transform.smoothscale(img,(int(XMUL*(img.get_width()/img.get_height())),int(YMUL)))
            for i in range(30):
                for j in range(80):
                    char=this.grid[i][j]
                    if char=='X':
                        this.surface.blit(this.wallImg,(int(XOFF+j*XMUL),int(YOFF+i*YMUL)))
                    elif char=='H':
                        this.surface.blit(this.ladderImg,(int(XOFF+j*XMUL),int(YOFF+i*YMUL)))
        
        #Check the number of players
        if len(playerList)<len([p for p in this.persons if p[2]=='P']) or len(donkeyList)>len([p for p in this.persons if p[2]=='D']):
            print("WARNING:improper number of persons in the level",end='.\t')
            if len(playerList)<len([p for p in this.persons if p[2]=='P']):
                print('More players required to play this level')
            else:
                print('Fewer donkeys required to play this level')
            exit(1)
        #Add extra players and donkeys if needed
        while len(playerList)>len([p for p in this.persons if p[2]=='P']):
            for i in range(1,79):
                if this.grid[28][i]==' ':
                    this.persons.append((i,28,'P'))
                    break
        while len(donkeyList)<len([p for p in this.persons if p[2]=='D']):
            for i in range(1,79):
                if this.grid[4][i]==' ' and this.grid[5][i] in ('X','H'):
                    donkeyList.append(person.Donkey('CPU',ImageLocation.donkey))
                    break
        
        #Keyboard bindings
        this.keymap={pygame.K_ESCAPE:(sys.exit,)}

        #Assign the persons
        for i in range(len(this.persons)):
            if this.persons[i][2]=='P':
                playerList[-1]
                this.persons[i][:2]
                playerList[-1].setDefault(list(this.persons[i][:2]))
                this.persons[i]=playerList.pop()
            else:
                donkeyList[-1].setDefault(list(this.persons[i][:2]))
                this.persons[i]=donkeyList.pop()
                
        #Assign the elements
        for i in range(len(this.elements)):
            if this.elements[i][2]=='C':
                this.elements[i]=element.Coin(this.coinImg,this.elements[i][0:2],(0,0))
            elif this.elements[i][2]=='O':
                this.elements[i]=element.FireBall(this.fireballImg,this.elements[i][0:2],(0,0))
            elif this.elements[i][2]=='Q':
                this.elements[i]=element.Trophy(this.trophyImg,this.elements[i][0:2],(0,0))

        #Bind the elements
        for e in this.elements:
            e.bindLevel(this)

        #Bind and setup the persons
        for p in this.persons:
            p.bindLevel(this)
            p.resetPosition()
            p.bindKeys(this.keymap)
        
        
    #Play the level/board
    def play(this):
        #reset all timers
        for p in this.persons:
            p.resetTime()
        for e in this.elements:
            e.resetTime()
        #game loop
        while not this.complete:
            #update surroundings of persons and elements
            for p in this.persons:
                p.checkWall()
            for e in this.elements:
                if type(e)==element.FireBall:
                    e.checkWall()
            
            #handle events
            for e in pygame.event.get():
                #user exits  (not useful in full screen)
                if e.type==pygame.QUIT:
                    sys.exit()
                #user presses a key
                elif e.type==pygame.KEYDOWN:
                    if e.key in this.keymap.keys():
                        this.keymap[e.key][0](True,*(this.keymap[e.key][1:]))
                #user releases a key
                elif e.type==pygame.KEYUP:
                    if e.key in this.keymap.keys():
                        this.keymap[e.key][0](False,*(this.keymap[e.key][1:]))

            #update the persons
            for p in this.persons:
                p.update()
            
            #update the elements
            for e in this.elements:
                e.update()

            #check for collisions
            for p in this.persons:
                if type(p)==person.Player:
                    for e in this.elements:
                        if e.isAlive() and e.isTouching(p.getPosition()):
                            if type(e)==element.Coin:
                                e.kill()
                                p.collectCoin()
                            elif type(e)==element.FireBall:
                                e.kill()
                                p.kill()
                            elif type(e)==element.Trophy:
                                e.kill()
                                p.collectCoin(50)
                                this.complete=True

            #remove fireballs at the bottom left
            for e in this.elements:
                if type(e)==element.FireBall and e.getPosition()[1]>27 and e.getPosition()[0]<2:
                    e.kill()

            #remove killed elements
            i=len(this.elements)-1
            while i>=0:
                if not this.elements[i].isAlive():
                    this.elements.pop(i)
                i-=1
                               
            #drawing
            this.draw()
            #update the screen
            display.flip()
            #manage FPS
            clock.tick(FPS)

        
    #draw the board
    def draw(this):
        #draw background
        screen.fill((155,50,50))
        
        #draw static board
        screen.blit(this.surface,(0,0))

        #draw persons
        for p in this.persons:
            p.draw()
        
        #draw the elements
        for e in this.elements:
            e.draw()

    #give surrounding info
    def giveSurrounding(this,pos):
        #Return a list representing the surroundings 
        #  8  
        #4 5 6
        #1 2 3
        #5 is true if the position is on a ladder
        #7 is true if the position is above a ladder adjacent to the floor
        #other numbers are true if there is a wall present in the corresponding direction
        toret=[False]*10

        #ladder check(5)
        if abs(int(pos[0]+0.3)-pos[0])<=0.3 and this.grid[int(pos[1]+0.95)][int(pos[0]+0.3)]=='H':
            toret[5]=True
            
        #check if standing above ladder adjacent to floor(7)
        if this.grid[int(pos[1]+0.1)+1][int(pos[0]+0.5)]=='H' and (this.grid[int(pos[1]+0.05)+1][int(pos[0]+0.5)-1]=='X' or this.grid[int(pos[1]+0.05)+1][int(pos[0]+0.5)+1]=='X'):
            toret[7]=True

        #check bottom position(2)
        if this.grid[int(pos[1])+1][int(pos[0]+0.3)]=='X' or this.grid[int(pos[1])+1][int(pos[0]+0.7)]=='X':
            toret[2]=True

        #check bottom-left position(1)
        if pos[1]-int(pos[1])<=0.1 and this.grid[int(pos[1])+1][int(pos[0]+0.2)] in ('X','H'):
            toret[1]=True

        #check bottom-right position(3)
        if pos[1]-int(pos[1])<=0.1 and this.grid[int(pos[1])+1][int(pos[0]+0.8)] in ('X','H'):
            toret[3]=True

        #check left position(4)
        if this.grid[int(pos[1]+0.05)][int(pos[0]+0.05)]=='X' or this.grid[int(pos[1]+0.9)][int(pos[0]+0.05)]=='X':
            toret[4]=True

        #check right position(6)
        if this.grid[int(pos[1]+0.05)][int(pos[0]+0.95)]=='X' or this.grid[int(pos[1]+0.9)][int(pos[0]+0.95)]=='X':
            toret[6]=True

        #check top position(8)
        if pos[1]-int(pos[1]-0.05)>=0.99 and (this.grid[int(pos[1]-0.05)][int(pos[0]+0.2)] or this.grid[int(pos[1]-0.05)][int(pos[0]+0.7)])=='X':
            toret[8]=True

        return toret
