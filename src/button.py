import pygame

class Button:
    """Creates a button taking in the button image and position\nSet the button action using the onPress method"""

    def __init__(this,img1=None,img2=None,pos=(0,0),size=(0,0)):
        if img1==None:
            print("Cannot create a button without an image")
        elif not type(img1)==pygame.Surface:
            print("Improper image format")
        this.activeimg=img1
        this.normalimg=img1
        this.highlightimg=img2
        this.pos=pos
        this.size=size
        this.run=None
        this.args=[]
    
    def onPress(this,func,*args):
        this.run=func
        this.args=args

    def action(this):
        if this.run==None:
            print("No action set for button")
        else:
            this.run(*this.args)

    def update(this,mouse):
        #get the position of the cursor
        pos=mouse.get_pos()
        #if cursor is out of the button
        if pos[0]<this.pos[0] or pos[0]>this.pos[0]+this.size[0] or pos[1]<this.pos[1] or pos[1]>this.pos[1]+this.size[1]:
            this.activeimg=this.normalimg
        #cursor is on the button
        else:
            if not this.highlightimg==None:
                this.activeimg=this.highlightimg
            if mouse.get_pressed()[0]:
                this.action()
            
    def draw(this,screen):
        if not type(screen)==pygame.Surface:
            print("Bad surface to draw button on")
        screen.blit(this.activeimg,this.pos)
