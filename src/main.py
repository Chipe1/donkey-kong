import pygame
from src.config import *
import src.button as button
import src.person as person
import src.board as board
import sys
def play():
    playerList=[]
    donkeyList=[]
    #setting up the buttons
    buttons=[]
    buttons.append(button.Button(pygame.transform.smoothscale(pygame.image.load("./res/play.png").convert_alpha(),(int(WIDTH*0.26041667),int(HEIGHT*0.13888889))),pygame.transform.smoothscale(pygame.image.load("./res/play_highlight.png").convert_alpha(),(int(WIDTH*0.26041667),int(HEIGHT*0.13888889))),(int(WIDTH*0.365),int(HEIGHT*0.463)),(int(WIDTH*0.26041667),int(HEIGHT*0.13888889))))
    buttons[-1].onPress(selectPlay,playerList,donkeyList)
    buttons.append(button.Button(pygame.transform.smoothscale(pygame.image.load("./res/exit.png").convert_alpha(),(int(WIDTH*0.26041667),int(HEIGHT*0.13888889))),pygame.transform.smoothscale(pygame.image.load("./res/exit_highlight.png").convert_alpha(),(int(WIDTH*0.26041667),int(HEIGHT*0.13888889))),(int(WIDTH*0.365),int(HEIGHT*0.787)),(int(WIDTH*0.26041667),int(HEIGHT*0.13888889))))
    buttons[-1].onPress(sys.exit)

    #add player 1
    playerList.append(person.Player(Player_1.name,Player_1.imgLocation,Player_1.controls))
    playerList[-1].changeColor(Player_1.color)
    #add player 2 if exists
    if Player_2.enable:
        playerList.append(person.Player(Player_2.name,Player_2.imgLocation,Player_2.controls))
        playerList[-1].setBoardPosition((60,37))
        playerList[-1].changeColor(Player_2.color)
    #if donkey 1 exists, add it
    if Donkey_1.enable:
        donkeyList.append(person.Donkey(Donkey_1.name,Donkey_1.imgLocation,Donkey_1.timePeriod,Donkey_1.controls))
        donkeyList[-1].setColor(Donkey_1.color)

    #if donkey 2 exists, add it
    if Donkey_2.enable:
        donkeyList.append(person.Donkey(Donkey_2.name,Donkey_2.imgLocation,Donkey_2.timePeriod,Donkey_2.controls))
        donkeyList[-1].setBarPosition((60,2))
        donkeyList[-1].setColor(Donkey_2.color)

    #game loop
    while True:
        #draw background
        screen.fill((155,50,50))
        
        #handle events
        for e in pygame.event.get():
            #user exits  (not useful in full screen)
            if e.type==pygame.QUIT:
                sys.exit()
            #user presses a key
            elif e.type==pygame.KEYDOWN:
                if e.key==pygame.K_ESCAPE:
                    sys.exit()
            #mouse press
            elif e.type==pygame.MOUSEBUTTONDOWN:
                #update buttons
                for b in buttons:
                    b.update(pygame.mouse)

        #update buttons
        for b in buttons:
            b.update(pygame.mouse)

        #draw buttons
        for b in buttons:
            b.draw(screen)

        #update the screen
        display.flip()

        #manage fps
        clock.tick(FPS)

def selectPlay(playerList,donkeyList):
    while True:
        for level in levelList:
            #multiply playerList and donkeyList to make a copy of depth 1
            game=board.Board(playerList*1,donkeyList*1,level)
            #play the game
            game.play()
