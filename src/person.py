import pygame
import math
import random
import src.element as element
from src.config import *

class Person:
    """Class for a human/computer controlled character"""
    def __init__(this,name,icon,controls=[None]*7):
        this.name=name
        this.icon = None
        if not test_mode:
            loaded=pygame.image.load(icon).convert_alpha()
            this.icon=pygame.transform.smoothscale(loaded,(int(XMUL*loaded.get_width()/loaded.get_height()),int(YMUL)))
        this.pos=[0,0]
        this.default=[0,0]
        this.state=0
        this.speed=list(SPEED)
        this.vel=[0,0]
        this.area=[0,0,int(XMUL),int(YMUL)]
        this.level=None
        this.key=controls
        this.time=pygame.time.get_ticks()
        this.surrounding=[]
        
    #return position of the person
    def getPosition(this):
        return this.pos*1

    #Set the default position
    def setDefault(this,default):
        this.default=default

    #reset position to default position
    def resetPosition(this):
        this.setPosition(this.default)

    #set the person postion to a specified value
    def setPosition(this,position):
        this.pos=position*1
    
    #draw the person
    def draw(this):
        if not type(screen)==pygame.Surface:
            print("bad surface to draw player '"+this.name+"' on")
        x,y=this.getPosition()
        screen.blit(this.icon,(int(XOFF+x*XMUL),int(YOFF+y*YMUL)),this.area)

    #bind the person to a level
    def bindLevel(this,level):
        this.level=level

    #reset the time
    def resetTime(this):
        this.time=pygame.time.get_ticks()

    #manage key presses
    def manageKey(this,state,key):
        print("manageKey not implemented for %s"%(this.name))

    #binds the keyboard keys to functions
    def bindKeys(this,keyDict):
        if not type(keyDict)==dict:
            print("Must bind keys to a dictionary")
        keyDict[this.key[0]]=(this.manageKey,'UP')
        keyDict[this.key[1]]=(this.manageKey,'LEFT')
        keyDict[this.key[2]]=(this.manageKey,'DOWN')
        keyDict[this.key[3]]=(this.manageKey,'RIGHT')
        keyDict[this.key[4]]=(this.manageKey,'JUMP')
        keyDict[this.key[5]]=(this.manageKey,'QUIT')
        keyDict[this.key[6]]=(this.manageKey,'POWER')

    #update the position
    def update(this):
        dt=(pygame.time.get_ticks()-this.time)/1000
        this.resetTime()
        this.pos[0]+=this.vel[0]*dt
        this.pos[1]+=this.vel[1]*dt

    #checks the surroundings for walls
    def checkWall(this):
        this.surrounding=this.level.giveSurrounding(this.getPosition())

    #function useful for debugging
    def printSurrounding(this):
        print(this.surrounding[7:9])
        print(this.surrounding[4:7])
        print(this.surrounding[1:4])
        print()


class Player(Person):
    """Player who tries to save the princess"""
    def __init__(this,name,icon,controls=(pygame.K_w,pygame.K_a,pygame.K_s,pygame.K_d,pygame.K_SPACE,pygame.K_q,pygame.K_e)):
        super(Player,this).__init__(name,icon,controls)
        #key = [ UP , LEFT , DOWN , RIGHT , JUMP , QUIT , POWER ]
        this.score=0
        this.life=3
        this.onLadder=False
        this.font=pygame.font.Font(None,32)
        this.textColor=(0,150,200)
        this.coinText=this.font.render(str(this.name)+' : '+str(this.score),True,this.textColor)
        this.lifeText=this.font.render('X'+str(this.life),True,this.textColor)
        this.boardPos=[5,37]
        
    #draw the player
    def draw(this):
        super(Player,this).draw()
        screen.blit(this.coinText,(this.boardPos[0]*XMUL,this.boardPos[1]*YMUL))
        screen.blit(this.lifeText,((this.boardPos[0]+2)*XMUL,(this.boardPos[1]+3)*YMUL))
        screen.blit(this.icon,(this.boardPos[0]*XMUL,(this.boardPos[1]+3)*YMUL),(0,0,XMUL,YMUL))
    
    #change text color
    def changeColor(this,newColor):
        this.textColor=tuple(newColor)
        this.coinText=this.font.render(str(this.name)+' : '+str(this.score),True,this.textColor)
        this.lifeText=this.font.render('X '+str(this.life),True,this.textColor)

    #set player score board position
    def setBoardPosition(this,newPos):
        this.boardPos=list(newPos)
    
    #player collects coin
    def collectCoin(this,points=5):
        Sound.coinCollect.play()
        this.score+=points
        if points>5:
            Sound.win.play()
            pygame.time.wait(2000)
        this.coinText=this.font.render(str(this.name)+' : '+str(this.score),True,this.textColor)

    #Player is hit by a fireball
    def kill(this):
        #player has lives left
        if this.life>1:
            Sound.burn.play()
            this.life-=1
            this.score=max(this.score-25,0)
            this.coinText=this.font.render('Score : '+str(this.score),True,this.textColor)
            this.lifeText=this.font.render('X '+str(this.life),True,this.textColor)
            this.resetPosition()
        else:
            Sound.gameOver.play()
            this.manageQuit(True)        
        
    #actions before closing the game
    def manageQuit(this,case):
        screen.fill((0,0,0))
        text=this.font.render(("GAME OVER" if case else "Bye")+"\nSCORE : "+str(this.score),True,this.textColor)
        screen.blit(text,(30*XMUL,30*YMUL))
        display.flip()
        pygame.time.wait(1000)
        for e in pygame.event.get():
            pass
        while True:
            for e in pygame.event.get():
                if e.type in (pygame.QUIT,pygame.KEYDOWN):
                    exit()

    #manage key presses
    def manageKey(this,state,key):
        if key=='LEFT':
            if state:
                this.state=2
                this.vel[0]=-this.speed[0]
            elif this.state==2:
                this.state=0
                this.vel[0]=0
        elif key=='RIGHT':
            if state:
                this.state=1
                this.vel[0]=this.speed[0]
            elif this.state==1:
                this.state=0
                this.vel[0]=0
        elif key=='JUMP' and state and not this.onLadder:
            if (this.surrounding[2] or this.surrounding[7]) and not this.surrounding[8]:
                this.vel[1]=-math.sqrt(2*G*H)#*0.95 #reduce velocity to account for the finitesimal value of dt
        elif key=='UP':
            if not this.onLadder:
                if state and this.surrounding[5] and (this.surrounding[2] or this.surrounding[7]):
                    this.onLadder=True
                    this.vel=[0,-this.speed[1]]
                    this.pos[0]=int(this.pos[0]+0.35)
            else:
                if state:
                    this.vel[1]=-this.speed[1]
                else:
                    this.vel[1]=max(0,this.vel[1])
        elif key=='DOWN':
            if not this.onLadder:
                if state and this.surrounding[7]:
                    this.onLadder=True
                    this.vel=[0,this.speed[1]]
                    this.pos[0]=int(this.pos[0]+0.35)
            else:
                if state:
                    this.vel[1]=this.speed[1]
                else:
                    this.vel[1]=min(0,this.vel[1])
        elif key=='QUIT':
            this.manageQuit(False)

    #override the update function
    def update(this):
        dt=(pygame.time.get_ticks()-this.time)/1000
        this.resetTime()
        #actions to do if not on ladder
        if not this.onLadder:
            if not (this.surrounding[2] or this.surrounding[7]):
                this.vel[1]+=G*dt
            #manage motion in y direction
            if ((this.surrounding[2] or this.surrounding[7])and not this.vel[1]<0) or (this.surrounding[8] and not this.vel[1]>0):
                this.pos[1]=int(this.pos[1]) if this.surrounding[2] else int(this.pos[1]+0.8)
                this.vel[1]=0
            else:
                this.pos[1]+=this.vel[1]*dt
            #manage motion in x direction
            if (this.surrounding[4] and not this.vel[0]>0) or (this.surrounding[6] and not this.vel[0]<0):
                this.pos[0]=int(this.pos[0]+0.8) if this.surrounding[4] else int(this.pos[0])
#               this.vel[0]=0
            else:
                this.pos[0]+=this.vel[0]*dt
        #if person is on ladder
        else:
            #get out of ladder if on ground
            if this.surrounding[2] and this.vel[1]>=0:
                this.onLadder=False
            #manage movement in x direction
            if (this.surrounding[2] or this.surrounding[7]) and this.vel[0]!=0 and this.pos[1]-int(this.pos[1]+0.05)<=0.05:
                this.onLadder=False
 #              this.vel[1]=0
            #manage movement in y direction
            if this.vel[1]!=0:
                #person in moving up
                if this.vel[1]<0 and not this.surrounding[8]:
                    if this.surrounding[5]:
                        this.pos[1]+=this.vel[1]*dt
                        #if out of ladder,set back onto ladder
                        this.checkWall()
                        if not (this.surrounding[5] or this.surrounding[7] or this.surrounding[2]):
                            this.pos[1]-=this.vel[1]*dt
                #person is moving down
                elif not this.surrounding[2]:
                    if this.surrounding[5] or this.surrounding[7]:
                        this.pos[1]+=this.vel[1]*dt
                        #if out of ladder,set back onto ladder
                        this.checkWall()
                        if not (this.surrounding[5] or this.surrounding[7] or this.surrounding[2]):
                            this.pos[1]-=this.vel[1]*dt
        #update the view area
        #if player is on ladder
        if this.onLadder:
            this.area[0]=XMUL*(8 if int(this.pos[1]/2)%2==0 else 9)
        #player is on ground
        elif this.surrounding[2] or this.surrounding[7]:
            #player is standing still
            if this.state ==0:
                this.area[0]=0
            #player is moving left
            elif this.state==2:
                this.area[0]=XMUL*(1 if int(this.pos[0])%2==0 else 2)
            #player is moving right
            else:
                this.area[0]=XMUL*(3 if int(this.pos[0])%2==0 else 4)
        #if player is in air
        else:
            if this.state==0:
                this.area[0]=XMUL*5
            elif this.state==2:
                this.area[0]=XMUL*6
            else:
                this.area[0]=XMUL*7            


class Donkey(Person):
    """Donkey Kong throws fireballs to hurt player(s)"""
    def __init__(this,name,icon,timePeriod=3,controls=None,fbicon=ImageLocation.fireball):
        if controls==None:
            controls=[controls]*3
        super(Donkey,this).__init__(name,icon,[None,controls[0],None,controls[1],None,None,controls[2]])
        #donkey must be larger
        if not test_mode:
            loaded=pygame.image.load(icon).convert_alpha()
            this.icon=pygame.transform.smoothscale(loaded,(int(XMUL*loaded.get_width()/loaded.get_height()*2),int(YMUL)*2))
        this.area=[0,0,2*XMUL,2*YMUL]
        if controls==[None]*3:
            this.AI=True
            this.lastAction=0
        else:
            this.AI=False
        this.timePeriod=timePeriod if timePeriod!=None else 3
        this.elapsed=this.timePeriod/2
        #donkey is slower
        this.speed=[x*0.8 for x in SPEED]
        this.fireImg = None
        if not test_mode:
            img=pygame.image.load(fbicon).convert_alpha()
            this.fireImg=pygame.transform.smoothscale(img,(int(XMUL*(img.get_width()/img.get_height())),int(YMUL)))
        this.bar=[XMUL,2*YMUL]
        this.color=(250,100,100)

    #set a new color
    def setColor(this,newColor):
        this.color=tuple(newColor)
    
    #set bar position
    def setBarPosition(this,newPosition):
        this.bar=[XMUL*newPosition[0],YMUL*newPosition[1]]
        
    #shoot a fireball
    def shootBall(this):
        this.level.elements.append(element.FireBall(this.fireImg,this.getPosition(),(BSPEED*(0.8+0.4*random.random())*(-1 if this.state==0 else 1),0)))
        this.level.elements[-1].bindLevel(this.level)
        this.elapsed=0
       
    #manage key presses
    def manageKey(this,state,key):
        if key=='LEFT':
            if state:
                this.state=0
                this.vel[0]=-this.speed[0]
            elif this.state==0:
                this.vel[0]=0
        elif key=='RIGHT':
            if state:
                this.state=1
                this.vel[0]=this.speed[0]
            elif this.state==1:
                this.vel[0]=0
        elif key=='POWER':
            if state:
                if this.elapsed>=this.timePeriod:
                    this.shootBall()

    #different draw function for donkey
    def draw(this):
        if not type(screen)==pygame.Surface:
            print("bad surface to draw player '"+this.name+"' on")
        x,y=this.getPosition()
        screen.blit(this.icon,(int(XOFF+x*XMUL),int(YOFF+(y-1)*YMUL)),this.area)
        #If player controlled, print the fire charge
        if not this.AI:
            pygame.draw.rect(screen,this.color,this.bar+[int(5*XMUL*this.elapsed/this.timePeriod),YMUL])
            pygame.draw.rect(screen,(100,12,137),this.bar+[5*XMUL,1*YMUL],3)

    
    #override the update function
    def update(this):
        dt=(pygame.time.get_ticks()-this.time)/1000
        this.resetTime()
        #manage movement along the x direction
        if ((this.surrounding[4] or not this.surrounding[1]) and this.vel[0]<0) or ((this.surrounding[6] or not this.surrounding[3]) and this.vel[0]>0):
            this.pos[0]=int(this.pos[0]+0.8) if this.surrounding[4] else int(this.pos[0])
            if this.AI:
                this.vel=[0,0]
        else:
            this.pos[0]+=this.vel[0]*dt
        #charge the fireball
        this.elapsed=min(this.elapsed+dt,this.timePeriod)
        #If not controlled by AI,transfer control to computer
        if this.AI:
            if int(this.time/1000)!=this.lastAction:
                r=random.randint(0,4)
                if r==1:
                    this.manageKey(True,'LEFT')
                elif r==2:
                    this.manageKey(True,'RIGHT')
                elif r==3:
                    this.manageKey(False,'Left')
                elif r==4:
                    this.manageKey(False,'RIGHT')
                this.lastAction=int(this.time/1000)
            #If fire ball is charged,fire it
            if this.elapsed>=this.timePeriod:
                this.shootBall()
        #update the view area
        #if just released a fireball
        if this.elapsed<=this.timePeriod/10:
            if this.state==0:
                this.area[0]=4*2*XMUL
            else:
                this.area[0]=5*2*XMUL
        #walking
        else:
            if this.state==0:
                if this.vel[0]==0:
                    this.area[0]=0
                else:
                    this.area[0]=2*XMUL*(0 if (this.time//170)%2==0 else 1)
            else:
                if this.vel[0]==0:
                    this.area[0]=2*2*XMUL
                else:
                    this.area[0]=2*XMUL*(2 if (this.time//170)%2==0 else 3)
