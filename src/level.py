import pygame
from sys.config import *

class Level:
    """contains information to play a level"""
    def __init__(this,levelfile,numPlayer):
        f=open(levelfile)
        #level surface
        this.surface=pygame.Surface(WIDTH,HEIGHT)
        #player list
        this.players=[]
        #element list
        this.elements=[]
        this.size=[int(x) for x in f.readline()]
        #level grid
        this.grid=f.readlines()
        #checking the grid size
        if(len(grid)!=this.size[1]):
            print(levelfile+" has improper level height")

        #resizing the grid to the exact size
        for i in range(len(this.grid)):
            line=this.grid[i]
            line=line[:len(line-1)]
            #line is longer than desired
            if len(line)>=this.size[0]:
                line=line[:this.size[0]]
            #line is shorter than required
            else:
                line=line[:]+" "*(this.size[0]-len(line))
            #check if the resize was successful
            if not len(line)==this.size[0]:
                print("Failed to resize line number ",i,"in "+levelfile)
            this.grid[i]=line
            
            #creating the level surface
            for j in range(len(this.grid[i])):
                char=this.grid[i][j]
                if char=="
        
