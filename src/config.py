#Contains all the configuration information
    
import pygame
import sys

#test mode determines whether we are testing the game or not
test_mode = True

#Control Scheme for players
# [ UP , LEFT , DOWN , RIGHT , JUMP , QUIT , ACTION ]

#Player 1 settings
class Player_1:
    enable = True
    name = 'Quasar'
    color = ( 100 , 255 , 100 )
    controls = ( pygame.K_w , pygame.K_a , pygame.K_s , pygame.K_d , pygame.K_SPACE , pygame.K_q , None )
    imgLocation = './res/blue.png'

#Player 2 settings
class Player_2:
    enable = True
    name = 'Luigi'
    color = ( 255 , 100 , 100 )
    controls = ( pygame.K_i , pygame.K_j , pygame.K_k , pygame.K_l , pygame.K_m , pygame.K_BACKSPACE , None )
    imgLocation = './res/blue.png'

#Control Scheme for donkeys
# [ LEFT , RIGHT , SHOOT ]

#Donkey 1 settings
class Donkey_1:
    enable = True
    name = 'Thopu'
    timePeriod = None
    color = ( 250 , 100 , 100 )
    controls = ( pygame.K_KP7 , pygame.K_KP9 , pygame.K_KP8 )
    imgLocation = './res/donkey.png'

#Donkey 2 settings
class Donkey_2:
    enable = False
    name = 'Basket Bullet'
    timePeriod = None
    color = ( 100 , 100 , 250 )
    controls = ( pygame.K_KP1 , pygame.K_KP3 , pygame.K_KP2 )
    imgLocation = './res/donkey.png'

#Image loactions
class ImageLocation:
    fireball = './res/fireball.png'
    wall = './res/wall.png'
    ladder = './res/ladder.png'
    coin = './res/coin.png'
    trophy = './res/star.png'
    donkey = './res/donkey.png'

#sounds
class Sound:
    coinCollect = './res/coinCollect.wav'
    burn = './res/burn.wav'
    gameOver = './res/gameOver.wav'
    win = './res/win.wav'

#List of levels to play
levelList = ( './res/levels/level1.dat' , './res/levels/level2.dat' , './res/levels/level3.dat' , './res/levels/level4.dat' )

#Screen resolution
WIDTH , HEIGHT = ( 0 , 0 )

#Frame rate of the game
FPS = 9999

#speed(in blocks per second) in x and y directions
SPEED = ( 6 , 6 )

#speed(blocks per second) of the fireball
BSPEED = 7

#acceleration due to gravity
G=32

#height of jump(in blocks)
H=2

#display adjustments

XOFF=0
YOFF=HEIGHT//9
XMUL=WIDTH/80
YMUL=HEIGHT/45

####################################
## DO NOT edit the following code ##
####################################

#Setup pygame the first time
if pygame.display.get_surface()==None:
    #setting up pygame (first called when main is run)
    if pygame.init()[1]!=0:
        print("Did not initialize properly")
    if not test_mode:
        pygame.display.set_mode((WIDTH,HEIGHT),pygame.FULLSCREEN)
    playerList=[]
    donkeyList=[]
    #make sure x 1 is enabled before x 2
    if Player_2.enable and not Player_1.enable:
        print('Enable Player 1 before enabling Player 2')
        sys.exit()
    if Donkey_2.enable and not Donkey_1.enable:
        print('Enable Donkey 1 before enabling Donkey 2')
        sys.exit()
        
    #make sure player 1 is enabled
    if not Player_1.enable:
        if Donkey.enable:
            print('You cant play as donkey without players')
        else:
            print('Need atleast one player')
        sys.exit()

#load sounds
Sound.coinCollect=pygame.mixer.Sound(Sound.coinCollect)   
Sound.burn=pygame.mixer.Sound(Sound.burn)
Sound.gameOver=pygame.mixer.Sound(Sound.gameOver)
Sound.win=pygame.mixer.Sound(Sound.win)
    
#variables common for all modules
display=pygame.display
screen=display.get_surface() 
WIDTH,HEIGHT= (800, 600) if test_mode else screen.get_size()
clock=pygame.time.Clock()

XOFF=0
YOFF=HEIGHT//9
XMUL=WIDTH/80
YMUL=HEIGHT/45
