import pygame
import random
from src.config import *

class Element:
    """Class for elements/particles used to check collision"""
    def __init__(this,icon,position,velocity,timePeriod=0.15):
        this.numStates= 1 if test_mode else int(icon.get_width()/icon.get_height())
        this.icon=icon
        this.level=None
        this.pos=list(position)
        this.vel=list(velocity)
        this.area=[0,0,XMUL,YMUL]
        this.state=random.randint(0,this.numStates-1)
        this.alive=True
        this.timePeriod=timePeriod
        this.time=pygame.time.get_ticks()
        this.surrounding=[False]*10

    #bind the element to a level
    def bindLevel(this,level):
        this.level=level
        this.checkWall()
        
    #get the position
    def getPosition(this):
        return this.pos*1

    #reset the time
    def resetTime(this):
        this.time=pygame.time.get_ticks()
        
    #check for collisions with walls
    def checkWall(this):
        this.surrounding=this.level.giveSurrounding(this.getPosition())
        
    def printSurrounding(this):
        print(this.surrounding[7:9])
        print(this.surrounding[4:7])
        print(this.surrounding[1:4])
        print()


    #update the element for animation
    def update(this):
        #update the state after the state change time
        if pygame.time.get_ticks()-this.time>=this.timePeriod*1000:
            this.state=(this.state+1)%this.numStates
            this.resetTime()
        #update the display area
        this.area[0]=this.state*XMUL

    #Kill the element
    def kill(this):
        this.alive=False

    #check if element is alive
    def isAlive(this):
        return this.alive
        
    #draw the element
    def draw(this):
        if not type(screen)==pygame.Surface:
            print("bad surface to draw element")
        x,y=this.getPosition()
        screen.blit(this.icon,(int(XOFF+x*XMUL),int(YOFF+y*YMUL)),this.area)

    def isTouching(this,pos):
        if abs(pos[0]-this.pos[0])>=0.8 or abs(pos[1]-this.pos[1])>=0.8:
            return False
        else:
            return True

class Coin(Element):
    pass

class Trophy(Element):
    pass

class FireBall(Element):
    """Fire balls thrown by monkeys"""
    #modify the init method
    def __init__(this,icon,position,velocity,timePeriod=0.15):
            super(FireBall,this).__init__(icon,position,velocity,timePeriod)
            this.lastFloor=this.getPosition()
            this.phyTime=pygame.time.get_ticks()

    #modify the resetTime method:
    def resetTime(this,which=True):
        if which:
            this.time=pygame.time.get_ticks()
        else:
            this.phyTime=pygame.time.get_ticks()

    #modify the update method
    def update(this):
        super(FireBall,this).update()
        dt=(pygame.time.get_ticks()-this.phyTime)/1000
        this.resetTime(False)
        #if ball is on ground
        if this.surrounding[2]:
            this.pos[1]=int(this.pos[1])
            #if it has no velocity(just landed on the ground),randomly give it one
            if this.vel[0]==0:
                this.vel=[BSPEED*(0.8+0.4*random.random())*(-1 if random.randint(0,1)==0 else 1),0]
            #if it collides with a wall,rebound it
            if (this.vel[0]>0 and this.surrounding[6]) or (this.vel[0]<0 and this.surrounding[4]):
                this.vel[0]*=-1
            #else let it move
            else:
                this.pos[0]+=this.vel[0]*dt
        #can fall through a ladder
        elif this.surrounding[7]:
            #check if first time on ladder this ladder
            if abs(this.pos[0]-this.lastFloor[0])>1 or abs(this.pos[1]-this.lastFloor[1])>1:
                this.lastFloor=this.getPosition()
                #if randint is 0, let the fireballs hit the floor
                if random.randint(0,1)==0:
                    this.vel[0]=0
                #fireball stays on the same floor
                else:
                    #if ball is already falling,stop it and assign a velocity
                    if this.vel[0]==0:
                        this.vel=[BSPEED*(0.8+0.4*random.random())*(-1 if random.randint(0,1)==0 else 1),0]
                    #else, let it roll normally
                    else:
                        pass
            #decision already made for this ladder
            else:
                #if falling,increase velocity
                if this.vel[0]==0:
                    this.vel[1]+=G*dt
                    this.pos[1]+=this.vel[1]*dt
                #if going horizantly,check for walls and move
                else:
                    if (this.vel[0]>0 and this.surrounding[6]) or (this.vel[0]<0 and this.surrounding[4]):
                        this.vel[0]*=-1
                    else:
                        this.pos[0]+=this.vel[0]*dt
        #freefall
        else:
            #if falling from horizontal motion, make it a vertical motion
            if this.vel[0]!=0:
                this.vel[0]=0
            this.vel[1]+=G*dt
            this.pos[1]+=this.vel[1]*dt
