import src.main
if src.main.test_mode:
	print('Please disable test mode in src/config.py to play the game')
	exit()
src.main.play()
exit()
