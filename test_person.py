import pytest
import src.person as person
import src.board as board
import src.element as element
import test_board

level=test_board.level

class Test_Player:
    "Class to test the player class"
    
    @pytest.fixture(scope='class')
    def mario(self, level):
        for i in level.persons:
            if type(i) == person.Player:
                return i

    def test_setDefault(self, mario):
        mario.setDefault([28,20])
        assert mario.default == [28, 20]
        
    def test_setPosition(self, mario):
        mario.setPosition((5,23))
        assert mario.pos == (5,23)
        
    def test_getPostion(self, mario):
        assert mario.getPosition() == (5,23)

    def test_resetPostion(self, mario):
        mario.resetPosition()
        assert mario.getPosition() == [28, 20]
                
    def test_kill(self, mario):
        mario.score = 11
        previous_lives=mario.life
        mario.kill()
        assert mario.life == (previous_lives - 1)
        #previous score is 11 so new score is 0
        assert mario.score == 0
        mario.score = 36
        previous_lives=mario.life
        mario.kill()
        assert mario.life == (previous_lives - 1)
        #previous score is 36 so new score is 11
        assert mario.score == 11

    def test_checkWall(self, mario):
        assert mario.surrounding == []
        mario.checkWall()
        assert len(mario.surrounding) == 10

    def test_update(self, mario):
        mario.setPosition([6,9])
        old_position=mario.getPosition()
        mario.update()
        new_position=mario.getPosition()
        assert new_position[0] == old_position[0] and new_position[1] >= old_position[1]


class Test_Donkey:
    "Class to test the donkey class"

    @pytest.fixture(scope='class')
    def donkey(self, level):
        for i in level.persons:
            if type(i) == person.Donkey:
                return i

    def test_manageKey(self, donkey):
        donkey.manageKey(True, 'LEFT')
        assert donkey.state == 0
        donkey.manageKey(True, 'RIGHT')
        assert donkey.state == 1
        donkey.manageKey(False, 'LEFT')
        assert donkey.vel[0] != 0
        donkey.manageKey(False, 'RIGHT')
        assert donkey.vel[0] == 0

    def test_shootBall(self, donkey, level):
        count = len(level.elements)
        donkey.shootBall()
        assert len(level.elements) == count+1
